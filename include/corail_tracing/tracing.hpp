// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#ifndef __TRACING__HPP_
#define __TRACING__HPP_

#include <string>

namespace corail {
namespace tracing {

    /**
     * @brief Report the beginning of an execution
     * 
     * @param sysname Name of the system
     * @param taskname Name of the task
     * @param release Release date of the execution
     * @param priority Priority of the task
     * @param clock The current date. If not specified take the current date from CLOCK_MONOTONIC
     */
    void trace_begin(std::string sysname, std::string taskname, uint64_t release, uint64_t priority, uint64_t clock);
    void trace_begin(std::string sysname, std::string taskname, uint64_t release, uint64_t priority);

    /**
     * @brief Report the end of an execution
     * 
     * @param sysname Name of the system
     * @param taskname Name of the task
     * @param release Realease date of the execution
     * @param priority Priority of the task
     * @param clock The current date. If not specified take the current date from CLOCK_MONOTONIC
     */
    void trace_end(std::string sysname, std::string taskname, uint64_t release, uint64_t priority, uint64_t clock);
    void trace_end(std::string sysname, std::string taskname, uint64_t release, uint64_t priority);


    /**
     * @brief Report state change.
     * 
     * @param state_name Name of the new state
     * @param taskname Name of the task
     * @param release Release date of the execution (if the state is an execution state)
     * @param clock Current date. If not specified take the current date from CLOCK_MONOTONIC
     */
    void state_begin(std::string state_name, std::string taskname, uint64_t release, uint64_t clock);
    void state_begin(std::string state_name, std::string taskname, uint64_t release);

    /**
     * @brief Report the triggering of a transition
     * 
     * @param transition_name Name of the transition
     * @param taskname Name of the task
     * @param clock Current date. If not specified take the current date from CLOCK_MONOTONIC
     */
    void trigger_transition(std::string transition_name, std::string taskname, uint64_t clock);
    void trigger_transition(std::string transition_name, std::string taskname);
}}

#endif