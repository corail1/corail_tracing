// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#undef TRACEPOINT_PROVIDER
#define TRACEPOINT_PROVIDER corail_tracing

#undef TRACEPOINT_INCLUDE
#define TRACEPOINT_INCLUDE "corail_tracing/corail-tp.h"

#if !defined(_TRACE_H) || defined(TRACEPOINT_HEADER_MULTI_READ)
#define _TRACE_H

#include <lttng/tracepoint.h>

TRACEPOINT_EVENT_CLASS(
    corail_tracing,
    trace,
    TP_ARGS(
        const char*, sysname_,
        const char*, taskname_,
        uint64_t, priority_,
        uint64_t, release_,
        uint64_t, clock_
    ),

    TP_FIELDS(
        ctf_string(sysname,sysname_)
        ctf_string(taskname,taskname_)
        ctf_integer(uint64_t, priority,priority_)
        ctf_integer(uint64_t, release,release_)
        ctf_integer(uint64_t, clock, clock_)
    )
)

TRACEPOINT_EVENT_INSTANCE(
    corail_tracing,
    trace,
    begin,
    TP_ARGS(
        const char*, sysname_,
        const char*, taskname_,
        uint64_t, priority_,
        uint64_t, release_,
        uint64_t, clock_
    )
)

TRACEPOINT_EVENT_INSTANCE(
    corail_tracing,
    trace,
    end,
    TP_ARGS(
        const char*, sysname_,
        const char*, taskname_,
        uint64_t, priority_,
        uint64_t, release_,
        uint64_t, clock_
    )
)

TRACEPOINT_EVENT(
    corail_tracing,
    state_begin,
    TP_ARGS(
        const char*, statename_,
        const char*, taskname_,
        uint64_t, clock_,
        uint64_t, release_
    ),

    TP_FIELDS(
        ctf_string(statename, statename_)
        ctf_string(taskname, taskname_)
        ctf_integer(uint64_t, clock, clock_)
        ctf_integer(uint64_t, release, release_)
    )
)

TRACEPOINT_EVENT(
    corail_tracing,
    trigger_transition,
    TP_ARGS(
        const char*, statename_,
        const char*, transition_,
        uint64_t, clock_
    ),

    TP_FIELDS(
        ctf_string(statename, statename_)
        ctf_string(transition, transition_)
        ctf_integer(uint64_t, clock, clock_)
    )
)

#endif /* _TRACE_H */

#include <lttng/tracepoint-event.h>
