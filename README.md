# Corail Tracing
Corail version 1.0, by Benoit Varillon and David Doose
and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

Corail_tracing contains tracing facilities for Corail.

Corail_tracing use LTTng (https://lttng.org/) to implements its functionalities.

For documentation and infromation about installation go to [corail1.gitlab.io](https://corail1.gitlab.io/)
