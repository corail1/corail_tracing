// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#include "corail_tracing/tracing.hpp"
#include "corail_tracing/corail-tp.h"

#include "pthread.h"


int64_t timespec_to_nsec(const timespec &time);
timespec get_time();

namespace corail {
namespace tracing {

    void trace_begin(std::string sysname, std::string taskname, uint64_t release, uint64_t priority)
    {
        tracepoint(corail_tracing, begin, sysname.c_str(), taskname.c_str(), priority, release, timespec_to_nsec(get_time()));
    }
    void trace_begin(std::string sysname, std::string taskname, uint64_t release, uint64_t priority, uint64_t clock)
    {
        tracepoint(corail_tracing, begin, sysname.c_str(), taskname.c_str(), priority, release, clock);
    }

    void trace_end(std::string sysname, std::string taskname, uint64_t release, uint64_t priority)
    {
        tracepoint(corail_tracing, end, sysname.c_str(), taskname.c_str(), priority, release, timespec_to_nsec(get_time()));
    }
    void trace_end(std::string sysname, std::string taskname, uint64_t release, uint64_t priority, uint64_t clock)
    {
        tracepoint(corail_tracing, end, sysname.c_str(), taskname.c_str(), priority, release, clock);
    }



    void state_begin(std::string state_name, std::string taskname, uint64_t release, uint64_t clock)
    {
        tracepoint(corail_tracing, state_begin, state_name.c_str(), taskname.c_str(), clock, release);
    }
    void state_begin(std::string state_name, std::string taskname, uint64_t release)
    {
        tracepoint(corail_tracing, state_begin, state_name.c_str(), taskname.c_str(), timespec_to_nsec(get_time()), release);
    }

    void trigger_transition(std::string transition_name, std::string taskname, uint64_t clock)
    {
        tracepoint(corail_tracing, trigger_transition, transition_name.c_str(), taskname.c_str(), clock);
    }
    void trigger_transition(std::string transition_name, std::string taskname)
    {
        tracepoint(corail_tracing, trigger_transition, transition_name.c_str(), taskname.c_str(), timespec_to_nsec(get_time()));
    }
}}

timespec get_time()
{
    timespec clock;
    clock_gettime(CLOCK_MONOTONIC, &clock);
    return clock;
}

int64_t timespec_to_nsec(const timespec &t)
{
    return t.tv_nsec + t.tv_sec * 1000000000;
}
